package slice

import "testing"

func TestSlice(t *testing.T) {
	got := Slice()
	want := []int{1,2,3}

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}