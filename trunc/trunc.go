package trunc

import (
	"fmt"
	"strconv"
)

func Trunc(number string) int {
	num, err := strconv.ParseFloat(number, 16)

	if err != nil {
		fmt.Println(err)
	}

	return int(num)
}
