package trunc

import "testing"

func TestTrunc(t *testing.T) {
	want := 20
	got := Trunc("20.01")

	if want != got {
		t.Errorf("got %d want %d", want, got)
	}
}
