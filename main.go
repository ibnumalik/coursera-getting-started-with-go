package main

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/ibnumalik/learn-go-with-tests/findian"
	"gitlab.com/ibnumalik/learn-go-with-tests/trunc"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	module := os.Args[1]

	switch module {
	case "trunc":
		fmt.Print("Enter any floating number: ")
		scanner.Scan()
		num := trunc.Trunc(scanner.Text())
		fmt.Printf("Truncated number is: %d", num)
	case "findian":
		fmt.Print("Enter a sentence: ")
		scanner.Scan()
		result := findian.Findian(scanner.Text())
		fmt.Printf(result)
	case "slice":
		for {
			fmt.Println("Enter a number, enter 'X' to exit.")
			scanner.Scan()
			input := scanner.Text()
			fmt.Printf("%q, %T", input, input)
			if input == "X" {
				break
			}
		}
	default:
		fmt.Printf("Command not found: %q", module)
	}
}
