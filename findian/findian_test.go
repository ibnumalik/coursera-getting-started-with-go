package findian

import "testing"

func TestFindian(t *testing.T) {
	words := []struct {
		want string
		word string
	}{
		{"Found!", "ian"},
		{"Found!", "Ian"},
		{"Found!", "iuiygaygn"},
		{"Found!", "I d skd a efju N"},
		{"Not Found!", "ihhhhhn"},
		{"Not Found!", "ina"},
		{"Not Found!", "xian"},
	}

	for i := 0; i < len(words); i++ {
		w := words[i]
		got := Findian(w.word)

		if got != w.want {
			t.Errorf("got %s want %s for %s", got, w.want, w.word)
		}
	}
}
