package findian

import "strings"

func Findian(word string) string {
	str := strings.ToLower(word)

	if strings.HasPrefix(str, "i") && strings.HasSuffix(str, "n") && strings.Contains(str, "a") {
		return "Found!"
	} else {
		return "Not Found!"
	}
}
